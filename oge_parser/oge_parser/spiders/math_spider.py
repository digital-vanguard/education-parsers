import re
import scrapy
from bs4 import BeautifulSoup


class MathSpiderSpider(scrapy.Spider):
    name = 'math_spider'
    start_urls = ['https://oge.sdamgia.ru/prob_catalog']
    custom_settings = {
        'FEED_EXPORT_ENCODING': 'utf-8',
    }

    def parse(self, response):
        for href in response.css('div.cat_children div.cat_category').xpath('.//a/@href').getall():
            course_url = response.urljoin(href)
            yield response.follow(course_url, callback=self.go_to_printable)

    def go_to_printable(self, response):
        printable_url = response.css('div.content').xpath('.//a[@target="_blank"]/@href').get()
        printable_url = response.urljoin(printable_url)
        yield response.follow(printable_url, callback=self.parse_category)

    def parse_category(self, response):
        def beautify_text(text):
            text = text.replace(' class="tex" ', '').replace('</p>', '\n')
            text = re.sub('<p.*?>', '', text)
            text = re.sub('style=".*?"', '', text)
            text = text.replace('­', '')
            text = text.replace('<!--auto generated from answers-->', '')
            return text

        def beautify_solution(text):
            text = re.sub('<div .*?>', '', text)
            text = text.replace('<b>Решение<!--rule_info-->.</b>', '')
            text = text.replace('<center>', '').replace('</center>', '')
            text = text.replace('<span >', '').replace('</span>', '').replace('<span>', '')
            text = text.replace('/div', '').replace('</p>', '\n')
            text = text.replace('­', '')
            text = re.sub('<p.*?>', '', text)
            text = text.replace(' class="tex" ', '')
            text = re.sub('style=".*?"', '', text)
            text = text.replace('<span >', '').replace('<>', '')
            return text

        def replace_urls(text):
            text = text.replace('https://oge.sdamgia.ru', '')
            begin = 'https://math-oge.sdamgia.ru'
            regex = re.compile('src=\\"/.*>')
            while len(re.findall(regex, text)) > 0:
                url = re.findall(regex, text)[0]
                text = re.sub(regex, 'src="' + begin + url[5:-1] + '>', text)
            return text

        catalog = {
            '1': 'Числа и вычисления',
            '2': 'Анализ диаграмм, таблиц, графиков',
            '3': 'Числовые неравенства, координатная прямая',
            '4': 'Числа, вычисления и алгебраические выражения',
            '5': 'Анализ диаграмм, таблиц, графиков',
            '6': 'Уравнения, неравенства и их системы',
            '7': 'Простейшие текстовые задачи',
            '8': 'Анализ диаграмм',
            '9': 'Статистика, вероятности',
            '10': 'Графики функций',
            '11': 'Арифметические и геометрические прогрессии',
            '12': 'Алгебраические выражения',
            '13': 'Расчеты по формулам',
            '14': 'Уравнения, неравенства и их системы',
            '15': 'Практические задачи по геометрии',
            '16': 'Треугольники, четырёхугольники, многоугольники и их элементы',
            '17': 'Окружность, круг и их элементы',
            '18': 'Площади фигур',
            '19': 'Фигуры на квадратной решётке',
            '20': 'Анализ геометрических высказываний'
        }

        results = dict()

        number = response.xpath('//div[@style="position:relative"]/div/div/span[@class="prob_nums"]/text()').get()
        results['number'] = ' '.join(number.split()[:2])
        if number:
            number = BeautifulSoup(number, features='lxml').get_text()
            if int(number.replace('\xa0', ' ').split(' ')[1]) > 20:
                return
            else:
                results['category'] = catalog[number.replace('\xa0', ' ').split(' ')[1]]
        else:
            return

        results['minor_category'] = response.css('div.new_header b::text').get()

        tasks = []

        for task in response.xpath('//div[@style="position:relative"]'):
            current_task = dict()

            current_task['id'] = task.xpath('.//div/div/span[@class="prob_nums"]/a/text()').get()

            text = ''.join(task.xpath('.//div/div[@class="pbody"]/p').getall())
            text = beautify_text(text)
            table = ''.join(task.xpath('.//div/div[@class="pbody"]//table').getall())
            if table:
                text += table
            current_task['text'] = replace_urls(text)

            text = ''.join(task.xpath('.//div/div[@class="nobreak solution"]').getall())
            text = beautify_solution(text)
            table = ''.join(task.xpath('.//div/div[@class="nobreak solution"]//table').getall())
            if table:
                text += table
            current_task['solution'] = replace_urls(text)

            try:
                current_task['answer'] = task.xpath('.//div/div[@class="answer"]//text()').get()[7:]
            except:
                current_task['answer'] = None

            if current_task['id']:
                tasks.append(current_task)

        results['tasks'] = tasks

        if results:
            yield {'results': results}
