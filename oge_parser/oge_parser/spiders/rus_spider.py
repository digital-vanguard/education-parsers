import re
import scrapy
from bs4 import BeautifulSoup


class MathSpiderSpider(scrapy.Spider):
    name = 'rus_spider'
    start_urls = ['https://rus-oge.sdamgia.ru/prob_catalog']
    custom_settings = {
        'FEED_EXPORT_ENCODING': 'utf-8',
    }

    def parse(self, response):
        for href in response.css('div.cat_main div.cat_category').xpath('.//a/@href').getall():
            course_url = response.urljoin(href)
            yield response.follow(course_url, callback=self.go_to_printable)

    def go_to_printable(self, response):
        printable_url = response.css('div.content').xpath('.//a[@target="_blank"]/@href').get()
        printable_url = response.urljoin(printable_url)
        yield response.follow(printable_url, callback=self.parse_category)

    def parse_category(self, response):
        def beautify_text(text):
            text = text.replace(' class="tex" ', '').replace('</p>', '\n')
            text = re.sub('<p.*?>', '', text)
            text = re.sub('style=".*?"', '', text)
            text = re.sub('<span.*?>', '', text)
            text = text.replace('­', '').replace('</span>', '')
            text = text.replace('<!--auto generated from answers-->', '')
            text = text.replace('<br>', '')
            text = text.replace('<b>', '').replace('</b>', '')
            text = text.replace('<u>', '').replace('</u>', '')
            text = text.replace('<i>', '').replace('</i>', '')
            return text

        def beautify_solution(text):
            text = re.sub('<div .*?>', '', text)
            text = text.replace('<b>Решение<!--rule_info-->.</b>', '')
            text = text.replace('<center>', '').replace('</center>', '')
            text = text.replace('/div', '').replace('</p>', '\n')
            text = text.replace('­', '')
            text = re.sub('<p.*?>', '', text)
            text = text.replace(' class="tex" ', '').replace('</span>', '')
            text = re.sub('style=".*?"', '', text)
            text = re.sub('<span.*?>', '', text)
            text = text.replace('<br>', '').replace('<>', '')
            text = text.replace('<b>', '').replace('</b>', '')
            text = text.replace('<!--rule_info-->', '')
            text = text.replace('<u>', '').replace('</u>', '')
            text = text.replace('<i>', '').replace('</i>', '')
            return text

        def replace_urls(text):
            text = text.replace('https://oge.sdamgia.ru', '')
            begin = 'https://rus-oge.sdamgia.ru'
            regex = re.compile('src=\\"/.*>')
            while len(re.findall(regex, text)) > 0:
                url = re.findall(regex, text)[0]
                text = re.sub(regex, 'src="' + begin + url[5:-1] + '>', text)
            return text

        catalog = {
            '2': 'Понимание смысла текста',
            '3': 'Анализ средств выразительности',
            '4': 'Правописание приставок',
            '5': 'Правописание суффиксов различных частей речи',
            '6': 'Синонимы. Фразеологические обороты',
            '7': 'Словосочетание',
            '8': 'Грамматическая (предикативная) основа предложения',
            '9': 'Осложнённое простое предложение',
            '10': 'Знаки препинания при вводных словах и обращениях',
            '11': 'Синтаксический анализ сложного предложения',
            '12': 'Знаки препинания в сложносочинённом и в сложноподчинённом предложениях',
            '13': 'Синтаксический анализ сложного предложения',
            '14': 'Сложные предложения с разными видами связи между частями',
        }

        results = dict()

        number = response.xpath('//div[@style="position:relative"]/div/div/span[@class="prob_nums"]/text()').get()
        results['number'] = ' '.join(number.split()[:2])
        if number:
            number = BeautifulSoup(number).get_text()
            if int(number.replace('\xa0', ' ').split()[1]) > 14 or int(number.replace('\xa0', ' ').split()[1]) < 2:
                return
            else:
                results['category'] = catalog[number.replace('\xa0', ' ').split(' ')[1]]
        else:
            return

        tasks = []

        for task in response.xpath('//div[@style="position:relative"]'):
            current_task = dict()

            current_task['id'] = task.xpath('.//div/div/span[@class="prob_nums"]/a/text()').get()

            text = ''.join(task.xpath('.//div/div[@class="pbody"]/p').getall())
            text = beautify_text(text)
            table = ''.join(task.xpath('.//div/div[@class="pbody"]//table').getall())
            if table:
                text += table

            text += ''.join(task.xpath('.//div/div[@class="probtext"]').getall())
            text = beautify_text(text)
            current_task['text'] = replace_urls(text)

            text = ''.join(task.xpath('.//div/div[@class="nobreak solution"]').getall())
            text = beautify_solution(text)
            table = ''.join(task.xpath('.//div/div[@class="nobreak solution"]//table').getall())
            if table:
                text += table
            current_task['solution'] = replace_urls(text)

            try:
                current_task['answer'] = task.xpath('.//div/div[@class="answer"]//text()').get()[7:]
            except:
                current_task['answer'] = None

            if current_task['id']:
                tasks.append(current_task)

        results['tasks'] = tasks

        if results:
            yield {'results': results}
